<%@ taglib prefix="html" uri="http://struts.apache.org/tags-html" %>
<%--
  Created by IntelliJ IDEA.
  User: vadym
  Date: 8/7/18
  Time: 11:05 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" language="java" %>
<html>
<head>
    <title>Add Customer</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="../css/style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<html:form action="/emmtesttask/saveCustomer">
    <div class="container">
        <html:hidden property="clientId" name="saveClientForm"/>
        <p>First name: <html:text name="saveClientForm" property="firstName" />
            <html:errors property="firstName"/></p>
        <p>Last name: <html:text name="saveClientForm" property="lastName" />
            <html:errors property="lastName" /></p>
        <p>Email: <html:text name="saveClientForm" property="email"/>
            <html:errors property="email"/></p>
        <p>Age: <html:text name="saveClientForm" property="date" styleId="datepicker"/></p>
                <html:errors property="date"/>
        <p>Sex: <html:select name="saveClientForm" property="sex">
            <html:option value="m">male</html:option>
            <html:option value="f">female</html:option>
        </html:select>
            <html:errors property="gender"/></p>
        <p>Account state: <html:text name="saveClientForm" property="accountState"/>
            <html:errors property="accountState"/></p>
        <div class="clearfix">
            <html:submit value="Save" property="${clientId}"/>
            <a href="../index.jsp">
                <input value="Back" type="button"/>
            </a>
        </div>
    </div>
</html:form>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function () {
        $("#datepicker").datepicker({dateFormat:'yy-mm-dd'}).val();
    });
</script>
</body>
</html>
