<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="html" uri="http://struts.apache.org/tags-html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="bean" uri="http://struts.apache.org/tags-bean" %>

<%--
  Created by IntelliJ IDEA.
  User: vadym
  Date: 8/9/18
  Time: 3:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Display clients</title>
    <link href="../css/style.css" rel="stylesheet" type="text/css"/>
</head>
<body>

<display:table id="data" name="sessionScope.displayClientsForm.clients" sort="list"
               requestURI="/emmtesttask/displayClients.do"
               pagesize="10" varTotals="totalValue">
    <display:column value="clientId" property="clientId" title="Id" sortable="true"/>
    <display:column property="firstName" title="Name" sortable="true"/>
    <display:column property="lastName" title="Last Name" sortable="true"/>

    <display:column property="email" title="Email" sortable="true">
        <html:link href="/emmtesttask/populate.do?clientId=${data.clientId}">
            ${data.email}
        </html:link>
    </display:column>


    <display:column property="age" title="age" sortable="true"/>
    <display:column property="sex" title="sex" sortable="true"/>
    <display:column property="accountState" title="account" total="true" sortable="true"/>
    <display:column>
        <a href="/emmtesttask/deleteCustomer.do?clientId=${data.clientId}">
            <button value="Delete">Delete</button>
        </a>
        <a href="/emmtesttask/populate.do?clientId=${data.clientId}">
            <button value="Update">Update</button>
        </a>
    </display:column>
    <display:footer>
    </display:footer>

</display:table>
<html:form action="/emmtesttask/displayClients">
    <html:text property="total" readonly="true"/>
</html:form>
<a href="/emmtesttask/saveCustomer.do?clientId=0">
    <button value="New Client">New Client</button>
</a>
</body>
</html>
