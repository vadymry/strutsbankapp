package org.rozdoum.common.form;

import org.apache.struts.action.ActionForm;
import org.rozdoum.common.model.Client;

import java.util.List;

public class ClientForm extends ActionForm {

    private int clientId;

    private int total;

    private List<Client> clients;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }
}
