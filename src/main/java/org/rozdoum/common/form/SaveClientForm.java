package org.rozdoum.common.form;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

public class SaveClientForm extends ActionForm {

    private int clientId;

    private String firstName;

    private String lastName;

    private String email;

    private String date;

    private String sex;

    private Integer accountState;

    public ActionErrors validate(ActionMapping mapping,
                                 HttpServletRequest request) {
        ActionErrors actionErrors = new ActionErrors();

        if (firstName == null || firstName.length() == 0) {
            actionErrors.add("firstName", new ActionMessage("error.no.name"));
        }
        if (lastName == null || lastName.length() == 0) {
            actionErrors.add("lastName", new ActionMessage("error.no.lastname"));
        }

        try {
            if (date == null || date.length() == 0) {
                actionErrors.add("date", new ActionMessage("error.no.date"));
            } else if (LocalDate.now().getYear() - LocalDate.parse(date).getYear() < 0) {
                actionErrors.add("date", new ActionMessage("error.invalid.date"));
            }
        } catch (Exception e) {
            actionErrors.add("date", new ActionMessage("error.invalid.dateformat"));
        }
        if (email == null || lastName.length() == 0) {
            actionErrors.add("email", new ActionMessage("error.no.email"));

        } else {
            try {
                InternetAddress validator = new InternetAddress(email);
                validator.validate();
            } catch (AddressException e) {
                actionErrors.add("email", new ActionMessage("error.not.valid.email"));
                e.printStackTrace();
            }
        }

        if (sex == null) {
            actionErrors.add("sex", new ActionMessage("error.no.gender"));
        }
        String regex = "\\d+";
        if (accountState == null) {
            actionErrors.add("accountState", new ActionMessage("error.no.accountState"));
        }

        return actionErrors;
    }


    public int getClientId() {
        return clientId;
    }

    public String getDate() {
        return date;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getSex() {
        return sex;
    }

    public Integer getAccountState() {
        return accountState;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setAccountState(Integer accountState) {
        this.accountState = accountState;
    }
}
