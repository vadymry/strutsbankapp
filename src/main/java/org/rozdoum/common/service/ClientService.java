package org.rozdoum.common.service;

import org.rozdoum.common.model.Client;

import java.util.List;

public interface ClientService {

    Client getClient(int clientId);

    List<Client> getAllClients();

    void deleteClient(int clientId);

    void saveClient(Client client);

    Integer getTotal();
}
