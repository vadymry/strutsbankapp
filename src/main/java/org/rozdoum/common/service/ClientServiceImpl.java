package org.rozdoum.common.service;

import org.rozdoum.common.dao.ClientDAOImpl;
import org.rozdoum.common.model.Client;

import java.util.List;

public class ClientServiceImpl implements ClientService {

    private ClientDAOImpl clientDAO = new ClientDAOImpl();


    @Override
    public Client getClient(int clientId) {
        return clientDAO.getClient(clientId);
    }

    @Override
    public List<Client> getAllClients() {
        return clientDAO.getAllClients();
    }

    @Override
    public void deleteClient(int clientId) {
        clientDAO.deleteClient(clientId);
    }

    @Override
    public void saveClient(Client client) {


        if(client.getClientId() == 0) {
            clientDAO.addClient(client);
        }else {
            clientDAO.updateClient(client);
        }
    }

    @Override
    public Integer getTotal(){
        return clientDAO.getTotalAccount();
    }
}
