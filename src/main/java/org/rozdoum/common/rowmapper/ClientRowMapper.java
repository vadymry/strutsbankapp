package org.rozdoum.common.rowmapper;

import org.rozdoum.common.model.Client;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientRowMapper implements RowMapper<Client> {

    @Override
    public Client mapRow(ResultSet resultSet, int i) throws SQLException {
        Client client = new Client();
        client.setClientId(resultSet.getInt("client_id"));
        client.setFirstName(resultSet.getString("first_name"));
        client.setLastName(resultSet.getString("last_name"));
        client.setEmail(resultSet.getString("email"));
        client.setAge(resultSet.getDate("age").toLocalDate());
        client.setSex(resultSet.getString("sex"));
        client.setAccountState(resultSet.getInt("account_state"));
        return client;
    }
}
