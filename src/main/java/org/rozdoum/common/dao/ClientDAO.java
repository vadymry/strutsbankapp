package org.rozdoum.common.dao;

import org.rozdoum.common.model.Client;

import java.util.List;

public interface ClientDAO {

    Client getClient(int userId);

    List<Client> getAllClients();

    void addClient(Client client)
            throws ClassNotFoundException;

    void updateClient(Client client);

    void deleteClient(int customerId);

    Integer getTotalAccount();

}
