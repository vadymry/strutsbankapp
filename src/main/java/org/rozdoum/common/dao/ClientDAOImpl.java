package org.rozdoum.common.dao;

import com.mysql.cj.jdbc.MysqlDataSource;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.rozdoum.common.model.Client;
import org.rozdoum.common.rowmapper.ClientRowMapper;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.io.InputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;


public class ClientDAOImpl implements ClientDAO {

    private static final Logger log = Logger.getLogger(ClientDAOImpl.class);

    private JdbcTemplate template;

    public ClientDAOImpl() {
        BasicConfigurator.configure();
        Properties properties = new Properties();
        try {
            InputStream in = getClass().getClassLoader()
                    .getResourceAsStream("settings.properties");
            properties.load(in);
        } catch (IOException e) {
            log.trace("No properties file found");
            e.printStackTrace();
        }
        MysqlDataSource dataSource = new MysqlDataSource();

        dataSource.setUrl(properties.getProperty("jdbc.url"));
        dataSource.setUser(properties.getProperty("jdbc.user"));
        dataSource.setPassword(properties.getProperty("jdbc.password"));
        template = new JdbcTemplate(dataSource);
    }

    @Override
    public Client getClient(int userId) {
        return template.queryForObject("SELECT * FROM clients WHERE client_id=?",
                new Object[]{userId}, new ClientRowMapper());
    }

    @Override
    public List<Client> getAllClients() {
        String getAllUsersStatement = "SELECT * FROM clients";
        return template.query(getAllUsersStatement, new BeanPropertyRowMapper<>(Client.class));
    }

    @Override
    public void addClient(Client client) {
        template.update("INSERT INTO clients(first_name, last_name, email, age, sex, account_state)" +
                        "VALUES(?, ?, ?, ?, ?, ?)", client.getFirstName(), client.getLastName(), client.getEmail(),
                client.getAge(), client.getSex(), client.getAccountState());
    }

    @Override
    public void updateClient(Client client) {
        template.update("UPDATE clients SET first_name=?, last_name=?, email=?, " +
                        "age=?, sex=?, account_state=? WHERE client_id=?",
                client.getFirstName(), client.getLastName(), client.getEmail(),
                client.getAge(), client.getSex(), client.getAccountState(), client.getClientId());
    }

    @Override
    public void deleteClient(int customerId) {
        template.update("DELETE FROM clients WHERE client_id=?", customerId);
    }

    @Override
    public Integer getTotalAccount() {
        log.debug("\nTotal account sum: " + template.queryForObject("SELECT SUM(account_state) FROM clients",
                Integer.class) + "\n");
        return template.queryForObject("SELECT SUM(account_state) FROM clients", Integer.class);
    }

}
