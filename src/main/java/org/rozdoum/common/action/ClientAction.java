package org.rozdoum.common.action;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.actions.MappingDispatchAction;
import org.rozdoum.common.dao.ClientDAOImpl;
import org.rozdoum.common.form.ClientForm;
import org.rozdoum.common.form.SaveClientForm;
import org.rozdoum.common.model.Client;
import org.rozdoum.common.service.ClientService;
import org.rozdoum.common.service.ClientServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ClientAction extends MappingDispatchAction {

    private ClientService clientService = new ClientServiceImpl();

    public ActionForward save(ActionMapping mapping, ActionForm form,
                              HttpServletRequest request, HttpServletResponse response) {
        SaveClientForm clientForm = (SaveClientForm) form;

        LocalDate date = LocalDate.parse(clientForm.getDate());
        Client client = new Client(clientForm.getClientId(), clientForm.getFirstName(),
                clientForm.getLastName(), clientForm.getEmail(), date,
                clientForm.getSex(), clientForm.getAccountState());
        clientService.saveClient(client);
        if(getErrors(request) == null || getErrors(request).size() ==0) {
            return mapping.findForward("success");
        } else{
            return mapping.getInputForward();
        }
    }

    public ActionForward display(ActionMapping mapping, ActionForm form,
                                 HttpServletRequest request, HttpServletResponse response) {

        List<Client> clients = clientService.getAllClients();

        ClientForm clientForm = (ClientForm) form;

        clientForm.setClients(clients);
        if (clientService.getTotal() != null) {
            clientForm.setTotal(clientService.getTotal());
        }else {
            clientForm.setTotal(0);
        }
        return mapping.findForward("success");
    }

    public ActionForward delete(ActionMapping mapping, ActionForm form,
                                HttpServletRequest request, HttpServletResponse response) {
        ClientForm clientForm = (ClientForm) form;

        int id = Integer.parseInt(String.valueOf(request.getParameter("clientId")));

        clientService.deleteClient(id);

        return mapping.findForward("success");
    }

    public ActionForward populate(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("clientId"));
        SaveClientForm clientForm = (SaveClientForm) form;
        Client client = clientService.getClient(id);
        clientForm.setClientId(client.getClientId());
        clientForm.setFirstName(client.getFirstName());
        clientForm.setLastName(client.getLastName());
        clientForm.setEmail(client.getEmail());
        //clientForm.setDate(String.valueOf(client.getAge())); //TODO think about what should be stored at DB
        clientForm.setSex(client.getSex());
        clientForm.setAccountState(client.getAccountState());
        return mapping.findForward("success");
    }
}
