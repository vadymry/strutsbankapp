
DROP DATABASE IF EXISTS db_bank;

CREATE DATABASE db_bank;

USE db_bank;

CREATE TABLE clients(
    client_id INT NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    email VARCHAR(50),
    age DATE,
    sex varchar(10),
    account_state REAL,
    PRIMARY KEY (client_id)
);
